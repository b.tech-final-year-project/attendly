import uuid

from flask import request, jsonify
from sqlalchemy.exc import IntegrityError

from apps.models.schema import Faculty, Course
from apps import app, db
from apps.controllers.admin.utils import admin_token_required


@app.route("/fac_all", methods=["GET"])
@admin_token_required
def get_all_faculties(current_admin):

    if not current_admin:
        return jsonify(
            {"status": "Error", "message": "Only admin can perform this action"}
        )

    faculties = Faculty.query.all()

    output = []

    for faculty in faculties:
        faculty_data = {}
        faculty_data["id"] = faculty.id
        faculty_data["email_id"] = faculty.email_id
        faculty_data["public_id"] = faculty.public_id
        faculty_data["name"] = faculty.name
        output.append(faculty_data)

    return jsonify({"status": "Success", "faculties": output})


@app.route("/faculty/<public_id>", methods=["GET"])
@admin_token_required
def get_one_faculty(current_admin, public_id):

    if not current_admin:
        return jsonify(
            {"status": "Error", "message": "Only admin can perform this action"}
        )

    faculty = Faculty.query.filter_by(public_id=public_id).first()
    if not faculty:
        return jsonify({"status": "Error", "message": "Faculty not found"})

    faculty_data = {}
    faculty_data["email_id"] = faculty.email_id
    faculty_data["public_id"] = faculty.public_id
    faculty_data["name"] = faculty.name
    faculty_data["admin"] = faculty.admin
    return jsonify({"status": "Success", "data": faculty_data})


@app.route("/faculty/<public_id>", methods=["DELETE"])
@admin_token_required
def delete_faculty(current_admin, public_id):

    if not current_admin:
        return jsonify({"status": "Error", "message": "Cannot perform that function"})

    faculty = Faculty.query.filter_by(public_id=public_id).first()

    if not faculty:
        return jsonify({"status": "Error", "message": "Faculty not found"})

    db.session.delete(faculty)
    db.session.commit()

    return jsonify({"status": "Success", "message": "faculty has been deleted"})


@app.route("/faculty/courses", methods=["GET"])
@admin_token_required
def get_all_courses(current_admin):

    if not current_admin:
        return jsonify({"status": "Error", "message": "Cannot perform that function"})

    courses = Course.query.all()

    output = []

    for course in courses:
        course_data = {}
        course_data["id"] = course.id
        course_data["code"] = course.code
        course_data["name"] = course.name
        course_data["instructor_id"] = course.instructor_id
        course_data["academic_session"] = course.academic_session
        course_data["trained"] = course.trained
        course.data["threshold"] = course.threshold
        output.append(course_data)

    return jsonify({"status": "Success", "courses": output})


@app.route("/requiredfacultycourses/<faculty_id>", methods=["GET"])
@admin_token_required
# admin route: gets all courses offered by the required faculty
def get_req_faculty_courses(current_admin, faculty_id):
    if not current_admin:
        return jsonify({"status": "Error", "message": "Cannot perform that function"})
    courses = Course.query.filter_by(instructor_id=faculty_id).all()

    if not courses:
        return jsonify({"status": "Success", "message": "No courses yet"})

    output = []

    for course in courses:
        course_data = {}
        course_data["id"] = course.id
        course_data["code"] = course.code
        course_data["name"] = course.name
        course_data["instructor_id"] = course.instructor_id
        course_data["academic_session"] = course.academic_session
        course_data["trained"] = course.trained
        output.append(course_data)

    return jsonify({"status": "Success", "courses": output})


@app.route("/faculty", methods=["POST"])
@admin_token_required
def create_new_faculty(current_admin):

    if not current_admin:
        return jsonify(
            {
                "status": "Error",
                "message": "Cannot perform that function. Only admin has access",
            }
        )

    data = request.get_json()

    # hashed_password = generate_password_hash(data['password'],method = 'sha256')

    new_faculty = Faculty(
        public_id=str(uuid.uuid4()),
        email_id=data["email_id"],
        name=data["name"],
        password=data["password"],
    )
    db.session.add(new_faculty)
    try:
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        return jsonify({"status": "Error", "message": "EmailID already exists!"})

    return jsonify({"status": "Success", "message": "New faculty Created!"})
