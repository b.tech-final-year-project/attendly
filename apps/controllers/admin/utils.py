import jwt

from functools import wraps
from flask import request, jsonify

from google.oauth2 import id_token
from google.auth.transport import requests

from apps.models.schema import Admin
from apps import app


# def admin_token_required(f):
#     @wraps(f)
#     def decorated(*args, **kwargs):
#         token = None

#         if "x-access-token" in request.headers:
#             token = request.headers["x-access-token"]

#         if not token:
#             return jsonify({"status": "Error", "message": "Token is missing"}), 401

#         try:
#             data = jwt.decode(token, app.config["SECRET_KEY"])
#             current_admin = Admin.query.filter_by(email_id=data["email_id"]).first()

#         except Exception:
#             return jsonify({"status": "Error", "message": "Token is invalid !"}), 401

#         return f(current_admin, *args, **kwargs)

#     return decorated


def admin_token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if "x-access-token" in request.headers:
            token = request.headers["x-access-token"]

        if not token:
            print("Error here")
            return jsonify({"status": "Error", "message": "Token is missing"}), 401

        try:
            CLIENT_ID = "724242133685-hgbm7kh1hc4kltsi99mpqneo06e75iif.apps.googleusercontent.com"
            idinfo = id_token.verify_oauth2_token(token, requests.Request(), CLIENT_ID)
            email_id = idinfo["email"]
            print("EMAIL!", email_id)
            current_admin = Admin.query.filter_by(email_id=email_id).first()

        except:
            return jsonify({"status": "Error", "message": "Token is invalid !"}), 401

        return f(current_admin, *args, **kwargs)

    return decorated