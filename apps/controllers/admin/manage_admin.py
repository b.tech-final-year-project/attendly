from flask import request, jsonify
from sqlalchemy.exc import IntegrityError

from apps import app, db
from apps.models.schema import Admin
from apps.controllers.admin.utils import admin_token_required


@app.route("/admin", methods=["POST"])
@admin_token_required
def create_admin(current_admin):

    if not current_admin:
        return jsonify(
            {
                "status": "Error",
                "message": "Cannot perform that function. Only admin has access",
            }
        )

    data = request.get_json()

    # hashed_password = generate_password_hash(data['password'],method = 'sha256')

    new_faculty = Admin(email_id=data["email_id"], name=data["name"], password=data["password"])
    db.session.add(new_faculty)
    try:
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        return jsonify({"status": "Error", "message": "EmailID already exists!"})

    return jsonify({"status": "Success", "message": "New Admin3 Created!"})


@app.route("/admins", methods=["GET"])
@admin_token_required
def get_all_admins(current_admin):
    admins = Admin.query.order_by(Admin.name.asc())

    output = []

    for admin in admins:
        admin_data = {}
        admin_data["id"] = admin.id
        admin_data["email"] = admin.email_id
        admin_data["name"] = admin.name
        output.append(admin_data)

    return jsonify({"status": "Success", "admins": output})


@app.route("/admin/<id>", methods=["DELETE"])
@admin_token_required
def delete_admin(current_admin, id):
    admin = Admin.query.filter_by(id=id).first()

    if not admin:
        return jsonify({"status": "Error", "message": "Admin not found"})

    db.session.delete(admin)
    db.session.commit()

    return jsonify({"status": "Success", "message": "faculty has been deleted"})
