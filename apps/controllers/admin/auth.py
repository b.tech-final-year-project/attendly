import jwt
import datetime

from flask import request, jsonify, make_response

from apps import app
from apps.models.schema import Admin
from apps.controllers.admin.utils import admin_token_required


@app.route("/login/admin", methods=["GET"])
@admin_token_required
def get_current_admin(current_admin):
    admin_data = {}
    admin_data["email_id"] = current_admin.email_id
    admin_data["name"] = current_admin.name
    return jsonify({"status": "Success", "data": admin_data})


@app.route("/admin_login", methods=["GET"])
def admin_login():
    auth = request.authorization

    if not auth or not auth.username or not auth.password:
        return make_response(
            jsonify({"message": "Could not verify"}),
            401,
            {"WWW-Authenticate": 'Basic realm = "Login required!"'},
        )

    admin = Admin.query.filter_by(email_id=auth.username).first()
    if not admin:
        return make_response(
            jsonify({"message": "Could not verify"}),
            401,
            {"WWW-Authenticate": 'Basic realm = "Login required!"'},
        )

    if admin.password == auth.password:
        token = jwt.encode(
            {
                "role": "admin",
                "email_id": admin.email_id,
                "exp": datetime.datetime.utcnow() + datetime.timedelta(minutes=180),
            },
            app.config["SECRET_KEY"],
        )

        return jsonify({"token": token.decode("UTF-8")})

    return make_response(
        jsonify({"message": "Could not verify"}),
        401,
        {"WWW-Authenticate": 'Basic realm = "Login required!"'},
    )


@app.route("/check_admin_login", methods=["GET"])
@admin_token_required
def check_admin_login(current_admin):

    admin = Admin.query.filter_by(email_id=current_admin.email_id).first()
    if not admin:
        return make_response(
            jsonify({"message": "Could not verify"}),
            401,
            {"WWW-Authenticate": 'Basic realm = "Login required!"'},
        )

    return make_response("Verified", 200, {"WWW-Authenticate": 'Basic realm = "Already logged in!"'})
