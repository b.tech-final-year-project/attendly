import os
import shutil
import pandas as pd

from flask import request, jsonify, url_for
from sqlalchemy.exc import IntegrityError

from apps import app, db
from apps.models.schema import Student, Photo
from apps.controllers.admin.utils import admin_token_required
from apps.vendor.facenet.facenet.src.align import align_single_cls


ALLOWED_EXTENSIONS = ("png", "jpg", "jpeg", "csv")
CLASSIFIER_IMAGES = app.config["CLASSIFIER_IMAGES"]
PROCESSED_FOLDER = app.config["PROCESSED_FOLDER"]


def allowed_file(filename):
    return "." in filename and filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route("/studentscsv", methods=["POST"])
@admin_token_required
def add_student_csv(current_admin):

    if not current_admin:
        return jsonify({"status": "Error", "message": "Cannot perform that function"})

    # check if the post request has the file part
    if "file" not in request.files:
        resp = jsonify({"Status": "Error", "message": "No file part in the request"})
        resp.status_code = 400
        return resp
    file = request.files["file"]
    if file.filename == "":
        resp = jsonify({"Status": "Error", "message": "No file selected for uploading"})
        resp.status_code = 400
        return resp
    if file and allowed_file(file.filename):
        filepath = os.path.join(app.config["UPLOAD_FOLDER"], file.filename)
        file.save(filepath)
        raw_data = pd.read_csv(filepath)
        df = pd.DataFrame(raw_data)
        df = df.dropna()
        added_students = []
        for index, each_tuple in df.iterrows():
            print(each_tuple[0])
            regno = each_tuple[0].upper()  # make regno column case insensitive
            name = each_tuple[1]
            email_id = each_tuple[2]
            new_student = Student(regno=regno, name=name, email_id=email_id)
            db.session.add(new_student)
            flag = 0
            try:
                db.session.commit()
            except IntegrityError:
                flag = 1
                db.session.rollback()
                return jsonify(
                    {
                        "status": "Error",
                        "message": "Registration number already exists!",
                        "Regno": regno,
                        "Added roll numbers": added_students,
                    }
                )
            if flag == 0:
                added_students.append(regno)

    else:
        resp = jsonify({"Status": "Error", "message": "Allowed file type is csv"})
        resp.status_code = 400
        return resp

    return jsonify(
        {
            "status": "Success",
            "message": "New students Created! Upload photos!",
            "Added roll numbers": added_students,
        }
    )


@app.route("/students", methods=["POST"])
@admin_token_required
def add_student(current_admin):

    if not current_admin:
        return jsonify({"status": "Error", "message": "Cannot perform that function"})

    data = dict(request.form)
    new_student = Student(
        regno=data["regno"][0].upper(),
        name=data["name"][0],
        email_id=data["email_id"][0],
    )  # make regno column case insensitive

    db.session.add(new_student)
    try:
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        return jsonify(
            {"status": "Error", "message": "Registration number already exists!"}
        )

    return jsonify(
        {"status": "Success", "message": "New student Created! Upload photos!"}
    )


@app.route("/students/photo", methods=["POST"])
def create_student_folder():
    # if not current_admin:
    #     return jsonify({"status": "Error", "message": "Cannot perform that function"})

    data = dict(request.form)
    regno = data["regno"][0].upper()
    student = Student.query.filter_by(regno=regno).first()
    if not student:
        return jsonify({"status": "Error", "message": "Student not found"})

    # creating a folder for the student
    directory = student.regno + "_" + (student.name).replace(" ", "_")

    aligned_images_path = PROCESSED_FOLDER

    try:
        os.mkdir(aligned_images_path)
    except FileExistsError:
        pass

    if "file" not in request.files:
        resp = jsonify({"Status": "Error", "message": "No file part in the request"})
        resp.status_code = 400
        return resp

    files = request.files.getlist("file")
    if len(files) != 0:
        for file in files:
            if file.filename == "":
                return (
                    jsonify(
                        {"Status": "Error", "message": "No file selected for uploading"}
                    ),
                    400,
                )

            elif not file.filename.lower().endswith(ALLOWED_EXTENSIONS):
                return (
                    jsonify({"Status": "Error", "message": "Extension not allowed"}),
                    400,
                )

        processed_imgs = align_single_cls.main(
            files, directory, aligned_images_path, 160, 32, True, 0.25, False
        )

        print(processed_imgs)

        for processed_img in processed_imgs:
            new_pic = Photo(uri=processed_img, student=student)
            db.session.add(new_pic)
            db.session.commit()

        return (
            jsonify({"Status": "Success", "message": "File successfully uploaded"}),
            201,
        )

    else:
        return (
            jsonify({"Status": "Error", "message": "No file selected for uploading"}),
            400,
        )


@app.route("/students/<regno>", methods=["DELETE"])
@admin_token_required
def delete_student(current_admin, regno):

    if not current_admin:
        return jsonify({"status": "Error", "message": "Cannot perform that function"})
    regno = regno.upper()
    print(regno)
    student = Student.query.filter_by(regno=regno).first()

    if not student:
        return jsonify({"status": "Error", "message": "Student not found"})

    # creating a folder for the student
    directory = student.regno + "_" + (student.name).replace(" ", "_")
    # Parent Directory path
    parent_dir = app.config["PROCESSED_FOLDER"]
    path = os.path.join(parent_dir, directory)

    db.session.delete(student)
    db.session.commit()

    # to delete folder
    # to delete photo db

    if os.path.exists(path) is True:
        shutil.rmtree(path)

    return jsonify({"status": "Success", "message": "Student has been deleted"})


@app.route("/studentphotos/<reg_no>", methods=["GET"])
def get_student_photos(reg_no):

    reg_no = reg_no.upper()
    student = Student.query.filter_by(regno=reg_no).first()

    if not student:
        return jsonify({"status": "Error", "message": "Student not found"})

    # student photos folder
    directory = student.regno + "_" + (student.name).replace(" ", "_")

    # Parent Directory path
    parent_dir = PROCESSED_FOLDER
    path = os.path.join(parent_dir, directory)
    print(path)
    if os.path.exists(path) is True:
        fileurls = []
        allfiles = os.listdir(path)
        for each_file in allfiles:
            updatefilename = os.path.join(directory, each_file)
            fileurls.append(url_for("download_file", filename=updatefilename))

        return jsonify({"Status": "Success", "Photo URLs": fileurls})

    else:
        return jsonify({"Status": "Success", "Message": "No photos added yet"})


# admin can view all tickets till date
