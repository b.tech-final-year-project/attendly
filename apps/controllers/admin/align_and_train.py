import os

from flask import jsonify

from apps import app, db
from apps.models.schema import Course
from apps.controllers.admin.utils import admin_token_required
from apps.vendor.facenet.facenet.src.align import align_dataset_mtcnn
from apps.vendor.facenet.facenet.src import classifier


ALLOWED_EXTENSIONS = ("png", "jpg", "jpeg", "csv")
CLASSIFIER_MODEL = app.config["CLASSIFIER_MODEL"]
CLASSIFIER_IMAGES = app.config["CLASSIFIER_IMAGES"]
THRESHOLD = 5


@app.route("/align_images/<course_id>", methods=["GET"])
@admin_token_required
def align_images(current_admin, course_id):

    if not current_admin:
        return (
            jsonify(
                {
                    "status": "Error",
                    "message": "Cannot perform that function. Only admin has access",
                }
            ),
            401,
        )

    course = Course.query.filter_by(id=course_id).first()
    directory = str(course.code)

    rawimages = os.path.join(CLASSIFIER_IMAGES, directory + "/raw")
    alignedimages = os.path.join(CLASSIFIER_IMAGES, directory + "/processed")

    align_dataset_mtcnn.main(rawimages, alignedimages, 160, 32, True, 0.25, False)
    return jsonify({"Status": "Success", "message": "All images aligned", "output": 1})
