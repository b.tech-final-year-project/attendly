from flask import request, jsonify
from sqlalchemy import and_

from apps import app, db
from apps.models.schema import (
    Student,
    Course,
    Ticket,
    Faculty,
    TicketStatus,
    faculty_ticket_assoc,
    student_ticket_assoc,
)
from apps.controllers.admin.utils import admin_token_required


@app.route("/faculty/ticket", methods=["GET"])
@admin_token_required
def view_faculty_tickets(current_admin):
    if not current_admin:
        return jsonify({"status": "Error", "message": "You need to be an admin"})

    output = []
    filters = [
        Ticket.id == faculty_ticket_assoc.c.ticket_id,
        faculty_ticket_assoc.c.faculty_id == Faculty.id,
        TicketStatus.status_code == Ticket.status_code,
        Course.id == Ticket.course_id,
    ]

    try:
        status_type = request.get_json()["status_type"]
    except Exception:
        status_type = ""

    if status_type != "" or None:
        """Filter based query.
        If nothing is set as the status_type, it will return every tickets"""
        filters.append(TicketStatus.status_type == status_type)

    tickets = (
        db.session.query(Ticket, faculty_ticket_assoc, Faculty, Course, TicketStatus)
        .filter(and_(*filters))
        .order_by(Ticket.id.desc())
        .all()
    )

    print(len(filters))
    print(tickets)

    if not tickets:
        return jsonify({"status": "Success", "message": "No tickets found"})

    output = []

    for ticket in tickets:
        ticket_data = {}
        ticket_data["ticket_id"] = ticket[0].id
        ticket_data["course_id"] = ticket[0].course_id
        ticket_data["course_name"] = ticket[4].name
        ticket_data["instructor_id"] = ticket[3].id
        ticket_data["instructor_name"] = ticket[3].name
        ticket_data["ticket_status"] = ticket[5].status_type
        ticket_data["issue"] = ticket[0].issue
        ticket_data["created_at"] = ticket[0].created_at
        ticket_data["updated_at"] = ticket[0].updated_at
        output.append(ticket_data)

    return jsonify({"status": "Success", "tickets": output})


@app.route("/student/ticket", methods=["GET"])
@admin_token_required
def view_student_tickets(current_admin):
    if not current_admin:
        return jsonify({"status": "Error", "message": "You need to be an admin"})

    output = []
    filters = [
        Ticket.id == student_ticket_assoc.c.ticket_id,
        student_ticket_assoc.c.student_id == Student.id,
        TicketStatus.status_code == Ticket.status_code,
        Course.id == Ticket.course_id,
    ]

    try:
        status_type = request.get_json()["status_type"]
    except Exception:
        status_type = ""

    if status_type != "" or None:
        """Filter based query.
        If nothing is set as the status_type, it will return every tickets"""
        filters.append(TicketStatus.status_type == status_type)

    tickets = (
        db.session.query(Ticket, student_ticket_assoc, Student, Course, TicketStatus)
        .filter(and_(*filters))
        .order_by(Ticket.id.desc())
        .all()
    )

    print(len(filters))
    print(tickets)

    if not tickets:
        return jsonify({"status": "Success", "message": "No tickets found"})

    print(tickets)
    output = []

    for ticket in tickets:
        ticket_data = {}
        ticket_data["ticket_id"] = ticket[0].id
        ticket_data["course_id"] = ticket[0].course_id
        ticket_data["course_name"] = ticket[4].name
        ticket_data["student_id"] = ticket[3].id
        ticket_data["student_name"] = ticket[3].name
        ticket_data["ticket_status"] = ticket[5].status_type
        ticket_data["issue"] = ticket[0].issue
        ticket_data["created_at"] = ticket[0].created_at
        ticket_data["updated_at"] = ticket[0].updated_at
        output.append(ticket_data)

    return jsonify({"status": "Success", "tickets": output})


@app.route("/faculty/ticket/<id>", methods=["PUT"])
@admin_token_required
def mark_faculty_ticket(current_admin, id):

    if not current_admin:
        return jsonify({"status": "Error", "message": "Cannot perform that function"})

    status_type = request.get_json()["status_type"]

    ticket = Ticket.query.filter_by(id=id).first()
    if not ticket:
        return jsonify({"status": "Error", "message": "Ticket not found"})

    ticket.status_code = status_type
    db.session.commit()

    return jsonify({"status": "Success", "message": "Ticket has been marked as solved"})


@app.route("/student/ticket/<id>", methods=["PUT"])
@admin_token_required
def mark_student_ticket(current_admin, id):

    if not current_admin:
        return jsonify({"status": "Error", "message": "Cannot perform that function"})

    status_type = request.get_json()["status_type"]

    ticket = Ticket.query.filter_by(id=id).first()
    if not ticket:
        return jsonify({"status": "Error", "message": "Ticket not found"})

    ticket.status_code = status_type
    db.session.commit()

    return jsonify({"status": "Success", "message": "Ticket has been marked as solved"})
