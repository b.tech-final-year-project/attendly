import os


from flask import (
    request,
    jsonify,
    send_from_directory,
)


from apps.models.schema import Student, Course, Attendance, marked
from apps import app, db
from apps.controllers.shared.utils import (
    fac_or_admin_token_required,
    fac_or_student_token_required,
)
from apps.vendor.facenet.facenet.src import classifier


CLASSIFIER_MODEL = app.config["CLASSIFIER_MODEL"]
CLASSIFIER_IMAGES = app.config["CLASSIFIER_IMAGES"]


@app.route("/")
def home_route():
    return jsonify({"message": "Welcome to attendly"}), 200


@app.route("/uploads/<path:filename>")
def download_file(filename):
    return send_from_directory(
        app.config["PROCESSED_FOLDER"], filename, as_attachment=True
    )


@app.route("/static/<filename>")
def download_static_file(filename):
    return send_from_directory(
        app.config["STATIC_FOLDER"], filename, as_attachment=True
    )


@app.route("/students", methods=["GET"])
@fac_or_admin_token_required
def get_all_students_for_admin_fac(admin_or_fac):
    students = Student.query.order_by(Student.name.asc())

    output = []

    for student in students:
        student_data = {}
        student_data["regno"] = student.regno
        student_data["name"] = student.name
        output.append(student_data)

    return jsonify({"status": "Success", "students": output})


# gives attendance statistics
@app.route("/attendance/statistics", methods=["POST"])
@fac_or_student_token_required
def student_attendance(fac_or_student):
    data = dict(request.form)
    course_id = data["id"]
    regno = data["regno"]

    course = Course.query.filter_by(id=course_id).first()

    if not course:
        return jsonify({"Status": "Error", "message": "Course does not exist"})

    """ Total attendance slots of the given course """
    total_slots = Attendance.query.filter_by(course_id=course_id).all()

    total_slot_set = {}
    for i in total_slots:
        total_slot_set[i.slot_id] = i

    total_count = list(total_slot_set.keys())

    enrolled_students = course.students
    if not enrolled_students:
        return jsonify(
            {"Status": "Error", "message": "Students have not been enrolled yet"}
        )

    student = Student.query.filter_by(regno=regno).first()
    if not student:
        return jsonify({"Status": "Error", "message": "Student does not exist"})

    if student not in enrolled_students:
        return jsonify({"Status": "Error", "message": "Student not enrolled"})
    else:

        count_dates = {}
        for eachslot in total_slot_set:
            attendance_entry = Attendance.query.filter_by(slot_id=eachslot).first()
            if attendance_entry.date_time.date() in count_dates:
                count_dates[attendance_entry.date_time.date()] += 1
            else:
                count_dates[attendance_entry.date_time.date()] = 1

        present_dates = []
        absent_dates = []

        """ Slot ids of present attendances of the student """
        present_slots = (
            Attendance.query.join(marked)
            .join(Student)
            .filter(
                (marked.c.attendance_id == Attendance.id)
                & (marked.c.student_id == Student.id)
                & (Attendance.course_id == course_id)
                & (marked.c.student_id == student.id)
            )
            .all()
        )

        present_slot_set = {}
        for i in present_slots:
            present_slot_set[i.slot_id] = i

        """ To get the absent slot set """
        absent_slot_set = {
            k: total_slot_set[k] for k in set(total_slot_set) - set(present_slot_set)
        }

        # print(len(present_slot_set))
        # print(len(total_slot_set))
        # print(len(absent_slot_set))

        present_count = len(present_slot_set.keys())

        present_dates = []
        for slot in present_slot_set:
            entry = present_slot_set[slot]
            present_dates.append(
                str(entry.date_time.date())
                + "_class"
                + str(count_dates[entry.date_time.date()])
            )
            count_dates[entry.date_time.date()] -= 1

        absent_dates = []
        for slot in absent_slot_set:
            entry = absent_slot_set[slot]
            absent_dates.append(
                str(entry.date_time.date())
                + "_class"
                + str(count_dates[entry.date_time.date()])
            )
            count_dates[entry.date_time.date()] -= 1

    return jsonify(
        {
            "Status": "Success",
            "total_count": len(total_count),
            "present_dates": present_dates,
            "present_count": present_count,
            "absent_dates": absent_dates,
        }
    )


@app.route("/train_model/<id>", methods=["POST"])
@fac_or_admin_token_required
def train_course_model(current_admin, id):

    if not current_admin:
        return (
            jsonify(
                {
                    "status": "Error",
                    "message": "Cannot perform that function. Only admin has access",
                }
            ),
            401,
        )

    data = request.get_json()

    print(data)

    if data["retrain"] != 1:
        print(jsonify({"Status": "Success", "message": "Model already trained"}))
        return jsonify({"Status": "Success", "message": "Model already trained"})

    course = Course.query.filter_by(id=id).first()
    coursename = course.name

    if not course:
        return jsonify({"Status": "Error", "message": "Invalid course"})

    # if course.trained is True:
    #     return jsonify({"Status": "Success", "message": "Model already trained"})

    # creating a folder for the student
    try:
        directory = str(course.id)
    except FileNotFoundError:
        return jsonify({"Status": "Course not found"}), 404

    # Parent Directory path
    classifierpath = os.path.join(CLASSIFIER_MODEL, directory)
    alignedimages = os.path.join(CLASSIFIER_IMAGES, directory)

    print(alignedimages)

    if os.path.exists(classifierpath) is False:
        os.mkdir(classifierpath)
    pklfile = str(course.id) + ".pkl"
    coursepkl = os.path.join(classifierpath, pklfile)
    f = open(coursepkl, "w")
    f.close()

    classifier.main(
        args_data_dir=alignedimages,
        args_classifier_filename=coursepkl,
        args_use_split_dataset=1,
        args_model=app.config["PRE_TRAINED_MODEL"],
        args_mode="TRAIN",
        args_batch_size=50,
        args_image_size=160,
        args_seed=666,
        args_min_nrof_images_per_class=5,
        args_nrof_train_images_per_class=5,
    )

    course.trained = True
    db.session.commit()

    print(
        jsonify(
            {
                "Status": "Success",
                "message": "Training complete",
                "CourseName": coursename,
            }
        ),
        200,
    )

    return (
        jsonify(
            {
                "Status": "Success",
                "message": "Training complete",
                "CourseName": coursename,
            }
        ),
        200,
    )
