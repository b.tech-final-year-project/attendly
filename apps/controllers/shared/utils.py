import jwt

from functools import wraps
from flask import request, jsonify

from google.oauth2 import id_token
from google.auth.transport import requests

from apps.models.schema import Admin, Faculty, Student
from apps import app


# def fac_or_admin_token_required(f):
#     @wraps(f)
#     def decorated(*args, **kwargs):
#         token = None

#         if "x-access-token" in request.headers:
#             token = request.headers["x-access-token"]

#         if not token:
#             return jsonify({"status": "Error", "message": "Token is missing"}), 401

#         try:
#             data = jwt.decode(token, app.config["SECRET_KEY"])

#             try:
#                 current_admin = Admin.query.filter_by(email_id=data["email_id"]).first()
#                 if current_admin:
#                     return f(current_admin, *args, **kwargs)

#             except KeyError as e:
#                 print(e)
#                 current_faculty = Faculty.query.filter_by(
#                     public_id=data["public_id"]
#                 ).first()
#                 if current_faculty:
#                     return f(current_faculty, *args, **kwargs)

#         except Exception:
#             return jsonify({"status": "Error", "message": "Token is invalid !"}), 401

#     return decorated


def fac_or_admin_token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if "x-access-token" in request.headers:
            token = request.headers["x-access-token"]

        if not token:
            return jsonify({"status": "Error", "message": "Token is missing"}), 401

        try:
            CLIENT_ID = "724242133685-hgbm7kh1hc4kltsi99mpqneo06e75iif.apps.googleusercontent.com"
            idinfo = id_token.verify_oauth2_token(token, requests.Request(), CLIENT_ID)
            email_id = idinfo["email"]

            try:
                current_admin = Admin.query.filter_by(email_id=email_id).first()
                if current_admin:
                    return f(current_admin, *args, **kwargs)

            except KeyError as e:
                print(e)
                current_faculty = Faculty.query.filter_by(email_id=email_id).first()
                if current_faculty:
                    return f(current_faculty, *args, **kwargs)

        except Exception:
            return jsonify({"status": "Error", "message": "Token is invalid !"}), 401

    return decorated


# def fac_or_student_token_required(f):
#     @wraps(f)
#     def decorated(*args, **kwargs):
#         token = None

#         if "x-access-token" in request.headers:
#             token = request.headers["x-access-token"]

#         if not token:
#             return jsonify({"status": "Error", "message": "Token is missing"}), 401

#         try:
#             data = jwt.decode(token, app.config["SECRET_KEY"])

#             try:
#                 current_student = Student.query.filter_by(
#                     email_id=data["email_id"]
#                 ).first()
#                 if current_student:
#                     return f(current_student, *args, **kwargs)

#             except KeyError as e:
#                 print(e)
#                 current_faculty = Faculty.query.filter_by(
#                     public_id=data["public_id"]
#                 ).first()
#                 if current_faculty:
#                     return f(current_faculty, *args, **kwargs)

#         except Exception:
#             return jsonify({"status": "Error", "message": "Token is invalid !"}), 401

#     return decorated


def fac_or_student_token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if "x-access-token" in request.headers:
            token = request.headers["x-access-token"]

        if not token:
            print("errror")
            return jsonify({"status": "Error", "message": "Token is missing"}), 401

        print("no errror")

        try:
            CLIENT_ID = "724242133685-hgbm7kh1hc4kltsi99mpqneo06e75iif.apps.googleusercontent.com"
            idinfo = id_token.verify_oauth2_token(token, requests.Request(), CLIENT_ID)
            email_id = idinfo["email"]

            try:
                current_student = Student.query.filter_by(email_id=email_id).first()
                if current_student:
                    return f(current_student, *args, **kwargs)

            except KeyError as e:
                print(e)
                current_faculty = Faculty.query.filter_by(email_id=email_id).first()
                if current_faculty:
                    return f(current_faculty, *args, **kwargs)

        except Exception:
            return jsonify({"status": "Error", "message": "Token is invalid !"}), 401

    return decorated


# def fac_or_student_or_admin_token_required(f):
#     @wraps(f)
#     def decorated(*args, **kwargs):
#         token = None

#         if "x-access-token" in request.headers:
#             token = request.headers["x-access-token"]

#         if not token:
#             return jsonify({"status": "Error", "message": "Token is missing"}), 401

#         try:
#             data = jwt.decode(token, app.config["SECRET_KEY"])

#             try:
#                 current_student = Student.query.filter_by(
#                     email_id=data["email_id"]
#                 ).first()
#                 if current_student:
#                     return f(current_student, *args, **kwargs)

#             except KeyError as e:
#                 print(e)
#                 try:
#                     current_faculty = Faculty.query.filter_by(
#                         public_id=data["public_id"]
#                     ).first()
#                     if current_faculty:
#                         return f(current_faculty, *args, **kwargs)
#                 except KeyError as e:
#                     current_admin = Admin.query.filter_by(
#                         email_id=data["email_id"]
#                     ).first()
#                     if current_admin:
#                         return f(current_admin, *args, **kwargs)

#         except Exception:
#             return jsonify({"status": "Error", "message": "Token is invalid !"}), 401

#     return decorated