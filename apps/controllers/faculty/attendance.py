import uuid
import os
import math

from flask import request, jsonify, url_for

from apps import app, db
from apps.models.schema import Attendance, marked, AttendancePhoto, Course, Student
from apps.controllers.faculty.utils import fac_token_required, allowed_file, send_mail
from apps.vendor.facenet.facenet.src import detect_and_classify

UPLOAD_FOLDER = app.config["UPLOAD_FOLDER"]
CLASSIFIER_MODEL = app.config["CLASSIFIER_MODEL"]
STATIC_FOLDER = app.config["STATIC_FOLDER"]


@app.route("/attendance", methods=["POST"])
# @fac_token_required
def take_attendance():
    data = dict(request.form)
    # print(data)
    id = data["id"][0]
    slotId = str(uuid.uuid4())
    # print(id)
    # check if the post request has the file part
    if "file" not in request.files:
        resp = jsonify({"Status": "Error", "message": "No file part in the request"})
        resp.status_code = 400
        return resp
    filelist = []
    files = request.files.getlist("file")
    counter = 0
    for file in files:
        if file.filename == "":
            resp = jsonify(
                {"Status": "Error", "message": "No file selected for uploading"}
            )
            resp.status_code = 400
            return resp
        if file and allowed_file(file.filename):
            counter = counter + 1
            filename = slotId + "_" + str(counter) + ".jpeg"
            filepath = os.path.join(UPLOAD_FOLDER, filename)
            file.save(filepath)
            new_pic = AttendancePhoto(uri=filepath, slot_id=slotId)
            db.session.add(new_pic)
            db.session.commit()

            filelist.append(UPLOAD_FOLDER + "/" + filename)
            resp = jsonify(
                {"Status": "Success", "message": "File successfully uploaded"}
            )
            resp.status_code = 201
        else:
            resp = jsonify(
                {"Status": "Error", "message": "Allowed file types are  png, jpg, jpeg"}
            )
            resp.status_code = 400
            return resp

    #     #enters only if file upload works

    course = Course.query.filter_by(id=id).first()

    if not course:
        return jsonify({"Status": "Error", "message": "Course does not exist"})

    pklfile = str(course.id) + "/" + str(course.id) + ".pkl"
    classifierpath = os.path.join(CLASSIFIER_MODEL, pklfile)

    print(filelist)

    output = detect_and_classify.main(
        classifierpath=classifierpath,
        slotid=slotId,
        imagepath=filelist,
        pretrained_model=app.config["PRE_TRAINED_MODEL"],
        class_threshold=course.threshold,
    )
    # print(output)

    present = []

    all_students = {}
    course = Course.query.filter_by(id=id).first()
    for student in course.students:
        all_students[student.regno] = {
            "emg_contact": student.emg_contact,
            "name": student.name,
        }

    absent = list(set(all_students.keys()) - set(output["regno"]))

    attendance_entry = Attendance(slot_id=slotId, course_id=id)

    for each_student in output["regno"]:
        stud = Student.query.filter_by(regno=each_student).first()
        print(stud)
        attendance_entry.students.append(stud)
        present.append(each_student)
        db.session.add(attendance_entry)

    db.session.commit()
    # print("All roll number", all_students.keys())
    # print("Absent", absent)
    # print("Present", present)

    status = 0
    for regno in all_students.keys():
        """ Function to send mail """
        if regno in absent:
            status = 0
        else:
            status = 1

        name = all_students[regno]["name"]
        emg_contact = all_students[regno]["emg_contact"]
        print("Sending mail", emg_contact)
        # send_mail(emg_contact, name, regno, course.name, status)

    cnt_present = len(present)
    cnt_absent = len(absent)
    print("Absentee count : ", cnt_absent, "Present Count : ", cnt_present)
    return jsonify(
        {
            "Status": "Success",
            "presentCount": cnt_present,
            "present_list": present,
            "slot_id": slotId,
            "absent_list": absent,
            "absentCount": cnt_absent,
        }
    )


@app.route("/deleteattendance/<slot_id>", methods=["DELETE"])
# @fac_token_required
def delete_attendance(slot_id):

    attendance_ids = Attendance.query.filter_by(slot_id=slot_id).all()

    if attendance_ids == []:
        return jsonify({"Status": "Error", "message": "No attendance entry found"})

    for eachid in attendance_ids:
        db.session.delete(eachid)
        db.session.commit()

    attendancephotos = AttendancePhoto.query.filter_by(slot_id=slot_id).all()
    if attendancephotos == []:
        return jsonify({"Status": "Error", "message": "No attendance photo found"})
    for pic_entry in attendancephotos:
        os.remove(pic_entry.uri)
        db.session.delete(pic_entry)
        db.session.commit()

    return jsonify({"Status": "Success", "message": "Attendance entry discarded"})


@app.route("/attendance/view", methods=["POST"])
# @fac_token_required
def view_oneclass_attendance():
    data = dict(request.form)
    id = data["id"]
    slot_id = data["slot_id"]

    course = Course.query.filter_by(id=id).first()

    if not course:
        return jsonify({"Status": "Error", "message": "Course does not exist"})

    all_students_objs = []
    course = Course.query.filter_by(id=id).first()
    for student in course.students:
        all_students_objs.append(student)

    attendance_record = Attendance.query.filter_by(
        course_id=id, slot_id=slot_id
    ).first()

    present_students = {}
    for stud in attendance_record.students:
        present_students[stud.regno] = 1

    output = []
    for each_record in all_students_objs:
        entry = {}
        entry["id"] = attendance_record.id
        entry["course_id"] = attendance_record.course_id
        entry["student_id"] = each_record.id
        entry["date_time"] = attendance_record.date_time
        entry["slot_id"] = attendance_record.slot_id
        entry["name"] = each_record.name
        entry["regno"] = each_record.regno
        if each_record.regno in present_students:
            entry["attendance_status"] = "present"
        else:
            entry["attendance_status"] = "absent"
        output.append(entry)

    return jsonify({"Status": "Success", "Class Attendance": output})


@app.route("/attendance/statistics/<course_id>", methods=["GET"])
# @token_required
def attendance_stats(course_id):
    course = Course.query.filter_by(id=course_id).first()

    attendancelist = Attendance.query.filter_by(course_id=course_id).all()

    if not attendancelist:
        return jsonify(
            {"Status": "Error", "message": "No attendance has been taken till date"}
        )

    if not course:
        return jsonify({"Status": "Error", "message": "Course does not exist"})

    total = (
        db.session.query(Attendance.slot_id)
        .distinct()
        .filter_by(course_id=course_id)
        .count()
    )

    required_class_count = math.floor(0.8 * total)
    enrolled_students = course.students
    if not enrolled_students:
        return jsonify(
            {"Status": "Error", "message": "Students have not been enrolled yet"}
        )

    output = []
    condonation = []
    no_condonation = []

    for each_student in enrolled_students:
        stud = {}
        stud["name"] = each_student.name
        stud["count_classes"] = len(
            Attendance.query.join(marked)
            .join(Student)
            .filter(
                (marked.c.attendance_id == Attendance.id)
                & (marked.c.student_id == Student.id)
                & (Attendance.course_id == course_id)
                & (marked.c.student_id == each_student.id)
            )
            .all()
        )
        output.append(stud)
        if stud["count_classes"] >= required_class_count:
            no_condonation.append(stud["name"])
        else:
            condonation.append(stud["name"])
    condonation.sort()
    no_condonation.sort()
    return jsonify(
        {
            "Status": "Success",
            "Total number of classes": total,
            "Attendance data": output,
            "80%": no_condonation,
            "<80%": condonation,
        }
    )


@app.route("/detectedfaces/<slot_id>", methods=["GET"])
# @fac_token_required
def detected_faces(slot_id):
    fileurls = []
    unknownfileurls = []
    allfiles = os.listdir(STATIC_FOLDER)
    for each_file in allfiles:
        if each_file[0:36] == slot_id:
            if each_file[37] == "U":
                print(each_file[37])
                unknownfileurls.append(url_for("static", filename=each_file))
            else:
                fileurls.append(url_for("static", filename=each_file))
    return jsonify(
        {
            "Status": "Success",
            "known_file_urls": fileurls,
            "unknown_file_urls": unknownfileurls,
        }
    )
