from apps import app, db
import jwt
from functools import wraps
from flask import Flask, request, jsonify, make_response, redirect, url_for
import datetime
from apps.models.schema import Student, Course, Ticket, TicketCreater, TicketStatus
from apps.controllers.faculty.utils import fac_token_required
import os
import collections
import shutil


ALLOWED_EXTENSIONS = ("png", "jpg", "jpeg", "csv")
THRESHOLD = 5


@app.route("/enroll_students", methods=["POST"])
@fac_token_required
def enroll_students(current_faculty):
    """ Functionalities for enrolment of students into a course """

    data = request.get_json()
    id = data["id"]
    course = Course.query.filter_by(id=id, instructor_id=current_faculty.id).first()

    if not course:
        return jsonify(
            {
                "Status": "Error",
                "message": "No such course or you are not the course instructor",
            }
        )

    invalid_regno = []

    # creating a folder for the course
    directory = str(course.id)
    # Parent Directory path
    to_path = os.path.join(app.config["CLASSIFIER_IMAGES"], directory)

    try:
        os.mkdir(to_path)
    except FileExistsError:
        print("Course folder already exists")

    # all student pics are stored here, after aligning the images
    parent_dir = app.config["PROCESSED_FOLDER"]

    less_number_cls = []
    less_number_flag = 0
    from_paths = []
    dst_paths = []

    for each_regno in data["regno"]:
        each_regno = each_regno.upper()
        student = Student.query.filter_by(regno=each_regno).first()
        if not student:
            invalid_regno.append(each_regno)
        else:
            course.students.append(student)
            stud_directory = student.regno + "_" + (student.name).replace(" ", "_")
            from_path = os.path.join(parent_dir, stud_directory)
            dst_path = os.path.join(to_path, stud_directory)
            from_paths.append(from_path)
            dst_paths.append(dst_path)

            """ Check if the number of images in any class meets the threshold """
            num_photos = len(
                [
                    f
                    for f in os.listdir(from_path)
                    if f.endswith(ALLOWED_EXTENSIONS)
                    and os.path.isfile(os.path.join(from_path, f))
                ]
            )
            if num_photos < THRESHOLD:
                less_number_flag = 1
                less_number_cls.append(stud_directory)

    if less_number_flag == 1:
        return (
            jsonify(
                {
                    "Status": "Failed",
                    "message": "Invalid number of images for classes : {}".format(
                        ",".join(less_number_cls)
                    ),
                    "CourseName": course.name,
                }
            ),
            200,
        )

    try:
        db.session.commit()
    except Exception as e:
        return jsonify(
            {
                "status": "Failure",
                "message": "Unexpected error while trying to insert into the db",
            }
        )

    """ Bug fix : Create symlink after committing """
    for i in range(0, len(from_paths)):
        try:
            """ Copy the photos from the source to course folder """
            os.symlink(from_paths[i], dst_paths[i])
        except FileExistsError:
            print(stud_directory, " already exists")

    if invalid_regno != []:
        return jsonify(
            {
                "Status": "Error",
                "message": "Following regnos are missing in db, the rest are enrolled",
                "data": invalid_regno,
            }
        )

    return jsonify({"Status": "Success", "message": "New students enrolled "})


@app.route("/enroll_students/<id>", methods=["GET"])
@fac_token_required
def get_enrolled_students(current_faculty, id):

    course = Course.query.filter_by(id=id, instructor_id=current_faculty.id).first()

    if not course:
        return jsonify({"Status": "Error", "message": "Invalid course"})

    student_dict = {}
    for each_student in course.students:
        student_dict[each_student.name] = each_student.regno

    if student_dict == {}:
        return jsonify({"Status": "Error", "message": "No enrolled students"})
    else:
        ordered_stud_dict = collections.OrderedDict(sorted(student_dict.items()))

    studentlist = []
    for entry in ordered_stud_dict:
        namedict = {}
        namedict["name"] = entry
        namedict["regno"] = ordered_stud_dict[entry]
        studentlist.append(namedict)

    model_status = course.trained
    return jsonify(
        {
            "Status": "Success",
            "students": studentlist,
            "model trained status": model_status,
        }
    )


@app.route("/enroll_students/<id>/<regno>", methods=["DELETE"])
@fac_token_required
def drop_enrolled_students(current_faculty, id, regno):

    course = Course.query.filter_by(id=id, instructor_id=current_faculty.id).first()

    if not course:
        return jsonify(
            {
                "Status": "Error",
                "message": "No such course or you are not the course instructor",
            }
        )

    regno = regno.upper()
    student = Student.query.filter_by(regno=regno).first()
    course_datatset_root = app.config["CLASSIFIER_IMAGES"]
    course_dataset_cls = os.path.join(course_datatset_root, str(course.id))
    if not student:
        return jsonify({"Status": "Error", "message": "Invalid regno"})

    else:
        if (
            student in course.students
        ):  # to check if the student had been enrolled to the course
            course.students.remove(student)
            """ Remove student directory from the course dataste folder """
            stud_directory = student.regno + "_" + (student.name).replace(" ", "_")
            student_path = os.path.join(course_dataset_cls, stud_directory)
            os.unlink(student_path)
            course.trained = 0
        else:
            return jsonify(
                {"Status": "Error", "message": "Student is not enrolled in course"}
            )

    db.session.commit()

    return jsonify({"Status": "Success", "message": "Student has been dropped "})


@app.route("/enrolling_done/<course_id>", methods=["GET"])
@fac_token_required
def enrollment_done(current_faculty, course_id):

    course = Course.query.filter_by(
        id=course_id, instructor_id=current_faculty.id
    ).first()
    course.trained = False
    print(course)

    if not course:
        return jsonify({"Status": "Error", "message": "Invalid course"})

    else:
        print("course exist")

    # same_ticket = Ticket.query.filter_by(instructor_id=current_faculty.id, course_id=course_id,
    #                                      issue="Students have been enrolled. Please create the model", pending=True).first()
    # if (not same_ticket):

    ticket_status = TicketStatus.query.filter_by(status_type="pending").first()
    ticket_creator = TicketCreater.query.filter_by(user_type="faculty").first()

    new_ticket = Ticket(
        course_id=course_id,
        issue="Students have been enrolled. Please create the model",
        status_code=ticket_status.status_code,
        created_by=ticket_creator.user_code,
    )
    new_ticket.faculties.append(current_faculty)
    db.session.add(new_ticket)
    db.session.commit()
    return jsonify(
        {
            "Status": "Success",
            "message": "Notification for training the model has been sent",
        }
    )

    # return jsonify({'Status': 'Success', 'message': 'Notification already sent'})
