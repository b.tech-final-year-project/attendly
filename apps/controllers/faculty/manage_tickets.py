from apps import app, db
import jwt
from functools import wraps
from flask import Flask, request, jsonify, make_response, redirect, url_for
import datetime
from apps.models.schema import (
    Ticket,
    Faculty,
    faculty_ticket_assoc,
    TicketStatus,
    Course,
    TicketCreater,
)
from apps.controllers.faculty.utils import fac_token_required
from sqlalchemy import and_

import os
import shutil
import collections


@app.route("/faculty/ticket", methods=["POST"])
@fac_token_required
def raise_faculty_ticket(current_faculty):
    data = dict(request.form)
    id = data["id"][0]
    issue = data["issue"][0]

    course = Course.query.filter_by(id=id, instructor_id=current_faculty.id).first()

    if not course:
        return jsonify({"Status": "Error", "message": "Course does not exist"})

    ticket_status = TicketStatus.query.filter_by(status_type="pending").first()
    ticket_creator = TicketCreater.query.filter_by(user_type="faculty").first()

    new_ticket = Ticket(
        course_id=id,
        issue=issue,
        status_code=ticket_status.status_code,
        created_by=ticket_creator.user_code,
    )
    new_ticket.faculties.append(current_faculty)
    db.session.add(new_ticket)
    db.session.commit()
    return jsonify({"Status": "Success", "message": "Ticket raised."})


@app.route("/faculty/ticket_retrain", methods=["POST"])
@fac_token_required
def raise_faculty_ticket_retrain(current_faculty):
    data = dict(request.form)
    id = data["id"][0]
    issue = data["issue"][0]

    course = Course.query.filter_by(id=id, instructor_id=current_faculty.id).first()
    course.trained = 0

    if not course:
        return jsonify({"Status": "Error", "message": "Course does not exist"})

    ticket_status = TicketStatus.query.filter_by(status_type="pending").first()
    ticket_creator = TicketCreater.query.filter_by(user_type="faculty").first()

    new_ticket = Ticket(
        course_id=id,
        issue=issue,
        status_code=ticket_status.status_code,
        created_by=ticket_creator.user_code,
    )
    new_ticket.faculties.append(current_faculty)
    db.session.add(new_ticket)
    db.session.commit()
    return jsonify({"Status": "Success", "message": "Ticket raised."})


@app.route("/faculty/view_ticket", methods=["GET"])
@fac_token_required
def view_faculty_tickets_by_fac_id(current_faculty):
    if not current_faculty:
        return jsonify({"status": "Error", "message": "You need to be an admin"})

    output = []
    filters = [
        Ticket.id == faculty_ticket_assoc.c.ticket_id,
        faculty_ticket_assoc.c.faculty_id == Faculty.id,
        TicketStatus.status_code == Ticket.status_code,
        Course.id == Ticket.course_id,
        Faculty.public_id == current_faculty.public_id,
    ]

    try:
        status_type = request.get_json()["status_type"]
    except Exception:
        status_type = ""

    if status_type != "" or None:
        """Filter based query.
        If nothing is set as the status_type, it will return every tickets"""
        filters.append(TicketStatus.status_type == status_type)

    tickets = (
        db.session.query(Ticket, faculty_ticket_assoc, Faculty, Course, TicketStatus)
        .filter(and_(*filters))
        .order_by(Ticket.id.desc())
        .all()
    )

    print(len(filters))
    print(tickets)

    if not tickets:
        return jsonify({"status": "Success", "message": "No tickets found"})

    output = []

    for ticket in tickets:
        ticket_data = {}
        ticket_data["ticket_id"] = ticket[0].id
        ticket_data["course_id"] = ticket[0].course_id
        ticket_data["course_name"] = ticket[4].name
        ticket_data["instructor_id"] = ticket[3].id
        ticket_data["instructor_name"] = ticket[3].name
        ticket_data["ticket_status"] = ticket[5].status_type
        ticket_data["issue"] = ticket[0].issue
        ticket_data["created_at"] = ticket[0].created_at
        ticket_data["updated_at"] = ticket[0].updated_at
        output.append(ticket_data)

    print(output)
    return jsonify({"status": "Success", "tickets": output})