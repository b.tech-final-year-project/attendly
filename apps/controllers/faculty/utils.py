import jwt
import smtplib
import datetime

from functools import wraps
from flask import Flask, request, jsonify, make_response, redirect, url_for

from google.oauth2 import id_token
from google.auth.transport import requests

from apps import app
from apps.models.schema import Faculty


ALLOWED_EXTENSIONS = set(["png", "jpg", "jpeg", "csv"])


def fac_token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if "x-access-token" in request.headers:
            token = request.headers["x-access-token"]

        if not token:
            print("Error here")
            return jsonify({"status": "Error", "message": "Token is missing"}), 401

        try:
            CLIENT_ID = "724242133685-hgbm7kh1hc4kltsi99mpqneo06e75iif.apps.googleusercontent.com"
            idinfo = id_token.verify_oauth2_token(token, requests.Request(), CLIENT_ID)
            email_id = idinfo["email"]
            print("EMAIL!", email_id)
            current_faculty = Faculty.query.filter_by(email_id=email_id).first()

        except:
            return jsonify({"status": "Error", "message": "Token is invalid !"}), 401

        return f(current_faculty, *args, **kwargs)

    return decorated


# def fac_token_required(f):
#     @wraps(f)
#     def decorated(*args, **kwargs):
#         token = None

#         if "x-access-token" in request.headers:
#             token = request.headers["x-access-token"]

#         if not token:
#             return jsonify({"status": "Error", "message": "Token is missing"}), 401

#         try:
#             data = jwt.decode(token, app.config["SECRET_KEY"])
#             current_faculty = Faculty.query.filter_by(public_id=data["public_id"]).first()

#         except:
#             return jsonify({"status": "Error", "message": "Token is invalid !"}), 401

#         return f(current_faculty, *args, **kwargs)

#     return decorated


def allowed_file(filename):
    return "." in filename and filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS


def send_mail(reciever_email, name, regno, course, status):

    sender_email = app.config["MAIL_USERNAME"]
    subject = "Attendance notice from NIT Calicut"
    if status == 0:
        body = "Hi, sorry to inform that {} ({}) has not attended the class {} today!".format(
            name, regno, course
        )
    else:
        body = "Hi, this is to inform that {} ({}) has attended the class {} today!".format(
            name, regno, course
        )
    port = app.config["SMTP_SSL_PORT"]

    email_text = "\r\n".join(
        [
            "From: {}".format(sender_email),
            "To: {}".format(reciever_email),
            "Subject: {}".format(subject),
            "",
            "{}".format(body),
        ]
    )
    try:
        server_ssl = smtplib.SMTP_SSL("smtp.gmail.com", port)
        server_ssl.ehlo()  # optional
        server_ssl.login(sender_email, app.config["MAIL_PASSWORD"])
        server_ssl.sendmail(sender_email, reciever_email, email_text)
        server_ssl.close()
        return jsonify({"Message": "Success! Email sent"})
    except:
        return jsonify({"Message": "Failed! Email not sent"})
