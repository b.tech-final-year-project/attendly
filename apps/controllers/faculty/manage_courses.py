import os
import shutil

from flask import request, jsonify

from apps import app, db
from apps.models.schema import Course
from apps.controllers.faculty.utils import fac_token_required


@app.route("/courses", methods=["GET"])
@fac_token_required
def get_faculty_courses(current_faculty):
    courses = Course.query.filter_by(instructor_id=current_faculty.id).all()

    if not courses:
        return jsonify({"status": "Success", "message": "No courses yet"})

    output = []

    for course in courses:
        course_data = {}
        course_data["id"] = course.id
        course_data["code"] = course.code
        course_data["name"] = course.name
        course_data["instructor_id"] = course.instructor_id
        course_data["academic_session"] = course.academic_session
        course_data["trained"] = course.trained
        course_data["threshold"] = course.threshold
        output.append(course_data)

    return jsonify({"status": "Success", "courses": output})


@app.route("/courses/<id>", methods=["GET"])
@fac_token_required
def get_one_course(current_faculty, id):
    course = Course.query.filter_by(id=id, instructor_id=current_faculty.id).first()

    if not course:
        return jsonify({"status": "Error", "message": "No such course found"})

    course_data = {}
    course_data["id"] = course.id
    course_data["code"] = course.code
    course_data["name"] = course.name
    course_data["instructor_id"] = course.instructor_id
    course_data["academic_session"] = course.academic_session
    course_data["trained"] = course.trained
    course_data["threshold"] = course.threshold
    return jsonify({"status": "Success", "course": course_data})


@app.route("/courses/<id>", methods=["PUT"])
@fac_token_required
def update_course(current_faculty, id):

    threshold = request.get_json()["threshold"]

    course = Course.query.filter_by(id=id).first()
    if not course:
        return jsonify({"status": "Error", "message": "Course not found"})

    course.threshold = float(threshold)

    if course.threshold > 1:
        return (
            jsonify(
                {
                    "status": "Failure",
                    "message": "Threshold too high",
                    "id": course.id,
                }
            ),
            400,
        )

    db.session.commit()
    return (
        jsonify(
            {
                "status": "Success",
                "message": "Course updated with new threshold",
                "id": course.id,
            }
        ),
        200,
    )


@app.route("/courses", methods=["POST"])
@fac_token_required
def create_course(current_faculty):
    data = request.get_json()

    new_course = Course(
        code=data["code"],
        name=data["name"],
        instructor_id=current_faculty.id,
        academic_session=data["academic_session"],
    )
    db.session.add(new_course)
    db.session.commit()
    return jsonify(
        {"status": "Success", "message": "New course created", "id": new_course.id}
    )


@app.route("/courses/<id>", methods=["DELETE"])
@fac_token_required
def delete_course(current_faculty, id):

    CLASSIFIER_IMAGES = app.config["CLASSIFIER_IMAGES"]

    course = Course.query.filter_by(id=id, instructor_id=current_faculty.id).first()

    if not course:
        return jsonify({"Status": "Error", "message": "No course found"})

    directory = str(course.id)
    to_path = os.path.join(CLASSIFIER_IMAGES, directory)

    try:
        shutil.rmtree(to_path)
    except OSError as e:
        print("Error: %s : %s" % (to_path, e.strerror))

    db.session.delete(course)
    db.session.commit()

    return jsonify({"status": "Success", "message": "Course has been deleted"})
