from apps import app
import jwt
from functools import wraps
from flask import Flask, request, jsonify, make_response, redirect, url_for
import datetime
from apps.models.schema import *
from apps.controllers.faculty.utils import fac_token_required


@app.route("/login/faculty", methods=["GET"])
@fac_token_required
def get_current_faculty(current_faculty):
    faculty_data = {}
    faculty_data["email_id"] = current_faculty.email_id
    faculty_data["public_id"] = current_faculty.public_id
    faculty_data["name"] = current_faculty.name
    return jsonify({"status": "Success", "data": faculty_data})


@app.route("/fac_login", methods=["GET"])
def faculty_login():
    auth = request.authorization

    if not auth or not auth.username or not auth.password:
        return make_response(
            jsonify({"message": "Could not verify"}),
            401,
            {"WWW-Authenticate": 'Basic realm = "Login required!"'},
        )

    faculty = Faculty.query.filter_by(email_id=auth.username).first()
    if not faculty:
        return make_response(
            jsonify({"message": "Could not verify"}),
            401,
            {"WWW-Authenticate": 'Basic realm = "Login required!"'},
        )

    if faculty.password == auth.password:
        token = jwt.encode(
            {
                "role": "faculty",
                "public_id": faculty.public_id,
                "exp": datetime.datetime.utcnow() + datetime.timedelta(minutes=180),
            },
            app.config["SECRET_KEY"],
        )

        return jsonify({"token": token.decode("UTF-8")})

    return make_response(
        jsonify({"message": "Could not verify"}),
        401,
        {"WWW-Authenticate": 'Basic realm = "Login required!"'},
    )


@app.route("/check_fac_login", methods=["GET"])
@fac_token_required
def check_fac_login(current_faculty):

    faculty = Faculty.query.filter_by(public_id=current_faculty.public_id).first()
    if not faculty:
        return make_response(
            jsonify({"message": "Could not verify"}),
            401,
            {"WWW-Authenticate": 'Basic realm = "Login required!"'},
        )

    return make_response("Verified", 200, {"WWW-Authenticate": 'Basic realm = "Already logged in!"'})
