import jwt
import datetime

from flask import request, jsonify, make_response

from apps import app
from apps.models.schema import Student
from apps.controllers.student.utils import student_token_required


@app.route("/student_login", methods=["GET"])
def student_login():
    auth = request.authorization

    if not auth or not auth.username or not auth.password:
        return make_response(
            jsonify({"message": "Could not verify"}),
            401,
            {"WWW-Authenticate": 'Basic realm = "Login required!"'},
        )

    student = Student.query.filter_by(regno=auth.username).first()
    if not student:
        return make_response(
            jsonify({"message": "Could not verify"}),
            401,
            {"WWW-Authenticate": 'Basic realm = "Login required!"'},
        )

    if student.password == auth.password:
        token = jwt.encode(
            {
                "role": "student",
                "email_id": student.email_id,
                "exp": datetime.datetime.utcnow() + datetime.timedelta(minutes=180),
            },
            app.config["SECRET_KEY"],
        )

        return jsonify(
            {
                "token": token.decode("UTF-8"),
                "data": {
                    "name": student.name,
                    "regno": student.regno,
                    "email_id": student.email_id,
                },
            }
        )

    return make_response(
        jsonify({"message": "Could not verify"}),
        401,
        {"WWW-Authenticate": 'Basic realm = "Login required!"'},
    )


@app.route("/check_student_login", methods=["GET"])
@student_token_required
def check_stud_login(current_student):

    student = Student.query.filter_by(email_id=current_student.email_id).first()
    if not student:
        return make_response(
            jsonify({"message": "Could not verify"}),
            401,
            {"WWW-Authenticate": 'Basic realm = "Login required!"'},
        )

    return make_response("Verified", 200, {"WWW-Authenticate": 'Basic realm = "Already logged in!"'})
