from flask import request, jsonify

from apps import app, db
from apps.models.schema import Course, Ticket, TicketStatus, TicketCreater
from apps.controllers.student.utils import student_token_required


@app.route("/student/ticket", methods=["POST"])
@student_token_required
def raise_student_ticket(current_student):

    data = dict(request.form)
    course_id = data["course_id"][0]
    issue = data["issue"][0]

    course = Course.query.filter_by(id=course_id).first()

    if not course:
        return jsonify({"Status": "Error", "message": "Course does not exist"})

    ticket_status = TicketStatus.query.filter_by(status_type="pending").first()
    ticket_creator = TicketCreater.query.filter_by(user_type="student").first()

    new_ticket = Ticket(
        course_id=course_id,
        issue=issue,
        status_code=ticket_status.status_code,
        created_by=ticket_creator.user_code,
    )
    new_ticket.students.append(current_student)
    db.session.add(new_ticket)
    db.session.commit()
    return jsonify({"status": "Success", "message": "Ticket raised!"}), 200
