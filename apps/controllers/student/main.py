from flask import jsonify

from apps import app, db
from apps.models.schema import Course, Student, enrolled, Attendance, marked
from apps.controllers.student.utils import student_token_required


@app.route("/student_courses", methods=["GET"])
@student_token_required
def get_enrolled_courses(current_student):
    enrolled_courses = []
    courses = (
        Course.query.join(enrolled)
        .join(Student)
        .filter(
            (enrolled.c.student_id == Student.id) & (enrolled.c.course_id == Course.id)
        )
        .all()
    )
    for course in courses:
        enrolled_courses.append(
            {
                "id": course.id,
                "name": course.name,
                "code": course.code,
                "instructor_id": course.instructor_id,
            }
        )
    print(jsonify({"Status": "Success", "courses": enrolled_courses}))
    return jsonify({"Status": "Success", "courses": enrolled_courses}), 200
