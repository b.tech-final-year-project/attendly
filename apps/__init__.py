# -*- coding: utf-8 -*-

from flask import Flask
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__, static_folder="static", template_folder="views")
app.config.from_pyfile("config.py")
db = SQLAlchemy(app, session_options={"autoflush": False})

from apps.models import *  # NOQA
from apps.controllers.student import *  # NOQA
from apps.controllers.faculty import *  # NOQA
from apps.controllers.admin import *  # NOQA
from apps.controllers.shared import *  # NOQA
from apps.controllers import *  # NOQA
