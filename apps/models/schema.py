from apps import db
import datetime
from werkzeug.security import generate_password_hash, check_password_hash


class Admin(db.Model):
    __tablename__ = "admin"

    id = db.Column(db.Integer, primary_key=True)
    email_id = db.Column(db.String(50), unique=True, nullable=False)
    name = db.Column(db.String(50), nullable=False)
    password = db.Column(db.String(80))

    def set_password(self, secret):
        self.password = generate_password_hash(secret)

    def check_password(self, secret):
        return check_password_hash(self.password, secret)


class Faculty(db.Model):
    __tablename__ = "faculty"

    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(50), unique=True)
    email_id = db.Column(db.String(50), unique=True, nullable=False)
    name = db.Column(db.String(50), nullable=False)
    password = db.Column(db.String(80))
    courses = db.relationship("Course", backref="faculty")

    def set_password(self, secret):
        self.password = generate_password_hash(secret)

    def check_password(self, secret):
        return check_password_hash(self.password, secret)


enrolled = db.Table(
    "enrolled",
    db.Column("student_id", db.Integer, db.ForeignKey("student.id"), primary_key=True),
    db.Column("course_id", db.Integer, db.ForeignKey("course.id"), primary_key=True),
)


class Course(db.Model):
    __tablename__ = "course"

    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(11))
    name = db.Column(db.String(50))
    academic_session = db.Column(db.String(50))
    instructor_id = db.Column(db.Integer, db.ForeignKey("faculty.id"))
    threshold = db.Column(db.Float, default=0.4)
    trained = db.Column(db.Boolean, default=False)
    tickets = db.relationship("Ticket", backref="course")


marked = db.Table(
    "marked",
    db.Column("attendance_id", db.Integer, db.ForeignKey("attendance.id"), nullable=False, primary_key=False),
    db.Column("student_id", db.Integer, db.ForeignKey("student.id"), nullable=False, primary_key=False),
)


class Student(db.Model):
    __tablename__ = "student"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    regno = db.Column(
        db.String(9),
        unique=True,
    )
    name = db.Column(db.String(50))
    email_id = db.Column(db.String(50), unique=True, nullable=False)
    password = db.Column(db.String(80))
    emg_contact = db.Column(db.String(80), nullable=True)
    photos = db.relationship("Photo", backref="student")
    courses = db.relationship("Course", secondary=enrolled, lazy="subquery", backref=db.backref("students", lazy=True))

    def set_password(self, secret):
        self.password = generate_password_hash(secret)

    def check_password(self, secret):
        return check_password_hash(self.password, secret)


class Attendance(db.Model):
    __tablename__ = "attendance"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    course_id = db.Column(db.Integer, db.ForeignKey("course.id"))
    date_time = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)
    slot_id = db.Column(db.String(50))
    students = db.relationship("Student", secondary=marked, backref=db.backref("attendances"))


class AttendancePhoto(db.Model):
    __tablename__ = "attendancephoto"

    id = db.Column(db.Integer, primary_key=True)
    uri = db.Column(db.Text)
    slot_id = db.Column(db.String(50))


class Photo(db.Model):
    __tablename__ = "photo"

    id = db.Column(db.Integer, primary_key=True)
    uri = db.Column(db.Text)
    student_id = db.Column(db.Integer, db.ForeignKey("student.id"))


class TicketStatus(db.Model):
    __tablename__ = "ticketstatus"

    id = db.Column(db.Integer, primary_key=True, nullable=False)
    status_code = db.Column(db.Integer, unique=True)
    status_type = db.Column(db.String(50))

    tickets = db.relationship("Ticket", backref="ticketstatus")


class TicketCreater(db.Model):
    __tablename__ = "ticketcreator"

    id = db.Column(db.Integer, primary_key=True, nullable=False)
    user_code = db.Column(db.Integer, unique=True)
    user_type = db.Column(db.String(50))

    tickets = db.relationship("Ticket", backref="ticketcreator")


class Ticket(db.Model):
    __tablename__ = "ticket"

    id = db.Column(db.Integer, primary_key=True)
    issue = db.Column(db.String(100))
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)
    updated_at = db.Column(db.DateTime, nullable=True)

    status_code = db.Column(db.Integer, db.ForeignKey("ticketstatus.status_code"), nullable=False)
    course_id = db.Column(db.Integer, db.ForeignKey("course.id"), nullable=False)
    created_by = db.Column(db.Integer, db.ForeignKey("ticketcreator.user_code"), nullable=False)

    students = db.relationship("Student", secondary="student_ticket_assoc", backref=db.backref("tickets"))
    faculties = db.relationship("Faculty", secondary="faculty_ticket_assoc", backref=db.backref("tickets"))


student_ticket_assoc = db.Table(
    "student_ticket_assoc",
    db.Column("ticket_id", db.Integer, db.ForeignKey("ticket.id"), nullable=False),
    db.Column("student_id", db.Integer, db.ForeignKey("student.id"), nullable=False),
)

faculty_ticket_assoc = db.Table(
    "faculty_ticket_assoc",
    db.Column("ticket_id", db.Integer, db.ForeignKey("ticket.id"), nullable=False),
    db.Column("faculty_id", db.Integer, db.ForeignKey("faculty.id"), nullable=False),
)
