-- MySQL dump 10.13  Distrib 8.0.22, for Linux (x86_64)
--
-- Host: localhost    Database: db_bfp
-- ------------------------------------------------------
-- Server version	8.0.22-0ubuntu0.20.04.3

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin` (
  `id` int NOT NULL AUTO_INCREMENT,
  `email_id` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `password` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_id` (`email_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'admin1@gmail.com','Admin1','admin1');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alembic_version`
--

DROP TABLE IF EXISTS `alembic_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alembic_version` (
  `version_num` varchar(32) NOT NULL,
  PRIMARY KEY (`version_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alembic_version`
--

LOCK TABLES `alembic_version` WRITE;
/*!40000 ALTER TABLE `alembic_version` DISABLE KEYS */;
INSERT INTO `alembic_version` VALUES ('4662f5c80a34');
/*!40000 ALTER TABLE `alembic_version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attendance`
--

DROP TABLE IF EXISTS `attendance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `attendance` (
  `id` int NOT NULL AUTO_INCREMENT,
  `course_id` int DEFAULT NULL,
  `date_time` datetime NOT NULL,
  `slot_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `course_id` (`course_id`),
  CONSTRAINT `attendance_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attendance`
--

LOCK TABLES `attendance` WRITE;
/*!40000 ALTER TABLE `attendance` DISABLE KEYS */;
INSERT INTO `attendance` VALUES (17,1,'2020-12-29 19:30:59','776081a5-376c-4a9b-aa88-2386179b612a'),(18,1,'2020-12-29 19:31:23','2834018f-6c10-4280-b699-b0e17f0627bc'),(19,1,'2020-12-29 19:43:46','cceb2dc0-4b7d-449a-972c-3ead2281dbd9'),(20,1,'2020-12-29 20:23:18','6c97d1ca-82ec-4884-930b-e6412c2ab5a5'),(21,1,'2020-12-29 20:23:44','26b02b70-294e-4beb-8a1f-a1e54b867295'),(22,1,'2020-12-29 20:31:30','2570d4e7-ea76-4432-ab9e-6d44160b9303'),(23,1,'2020-12-29 20:33:49','9995b525-d7cc-45fa-b2c9-df43803de034'),(24,1,'2020-12-29 20:35:28','a55fb369-5226-466a-9b36-85a4ed8220ec'),(25,1,'2020-12-29 21:01:27','8bc96b9d-629e-4bfe-9456-85dd402affcc'),(26,1,'2020-12-29 21:02:31','4e02884b-8e8c-41a1-8d50-918f912f8c12'),(28,1,'2020-12-31 00:09:09','2e158016-81c6-4489-89ab-79fad963f524');
/*!40000 ALTER TABLE `attendance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attendance_photo`
--

DROP TABLE IF EXISTS `attendance_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `attendance_photo` (
  `id` int NOT NULL AUTO_INCREMENT,
  `uri` text,
  `slot_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attendance_photo`
--

LOCK TABLES `attendance_photo` WRITE;
/*!40000 ALTER TABLE `attendance_photo` DISABLE KEYS */;
INSERT INTO `attendance_photo` VALUES (5,'/home/shady/Works/attendlyres/attUploads/4e8339fe-9fef-46b2-965e-cae9226e1132_1.jpeg','4e8339fe-9fef-46b2-965e-cae9226e1132'),(6,'/home/shady/Works/attendlyres/attUploads/e05b23e0-babd-4429-933e-8d3b1c70e002_1.jpeg','e05b23e0-babd-4429-933e-8d3b1c70e002'),(7,'/home/shady/Works/attendlyres/attUploads/2f4811e3-0b1b-4041-8b00-9853332cbee8_1.jpeg','2f4811e3-0b1b-4041-8b00-9853332cbee8'),(8,'/home/shady/Works/attendlyres/attUploads/fe0c5fbc-eade-46a6-8539-63f7616781e1_1.jpeg','fe0c5fbc-eade-46a6-8539-63f7616781e1'),(9,'/home/shady/Works/attendlyres/attUploads/d187b059-1e25-4d43-80b8-6259bbc5ef90_1.jpeg','d187b059-1e25-4d43-80b8-6259bbc5ef90'),(10,'/home/shady/Works/attendlyres/attUploads/93fb44e3-449c-44c2-bbfc-5559ced9fb9c_1.jpeg','93fb44e3-449c-44c2-bbfc-5559ced9fb9c'),(11,'/home/shady/Works/attendlyres/attUploads/34736564-2c6c-4e22-9387-8e0cd1ac2d7e_1.jpeg','34736564-2c6c-4e22-9387-8e0cd1ac2d7e'),(12,'/home/shady/Works/attendlyres/attUploads/f6411c66-0456-4730-9659-af468158983e_1.jpeg','f6411c66-0456-4730-9659-af468158983e'),(13,'/home/shady/Works/attendlyres/attUploads/9dbf079e-82fe-4ec0-9108-d1b3969ec40d_1.jpeg','9dbf079e-82fe-4ec0-9108-d1b3969ec40d'),(14,'/home/shady/Works/attendlyres/attUploads/8a401283-1e29-4890-9ead-6f5170f63e60_1.jpeg','8a401283-1e29-4890-9ead-6f5170f63e60'),(15,'/home/shady/Works/attendlyres/attUploads/5fce0089-89b4-467d-8f17-0c073d4ea45d_1.jpeg','5fce0089-89b4-467d-8f17-0c073d4ea45d'),(16,'/home/shady/Works/attendlyres/attUploads/c5db20bf-e63f-4be9-a798-4d1fbb23bcc8_1.jpeg','c5db20bf-e63f-4be9-a798-4d1fbb23bcc8'),(17,'/home/shady/Works/attendlyres/attUploads/305ba8e9-3c40-4a25-b0a7-d2ce79678f6f_1.jpeg','305ba8e9-3c40-4a25-b0a7-d2ce79678f6f'),(18,'/home/shady/Works/attendlyres/attUploads/4a9ec688-8045-42ac-a01d-4dd396bc3ccc_1.jpeg','4a9ec688-8045-42ac-a01d-4dd396bc3ccc'),(19,'/home/shady/Works/attendlyres/attUploads/25f886bc-82b1-45a4-867b-1f372e9d74bb_1.jpeg','25f886bc-82b1-45a4-867b-1f372e9d74bb'),(20,'/home/shady/Works/attendlyres/attUploads/86fa317b-d6be-4355-988a-7e736eb40c12_1.jpeg','86fa317b-d6be-4355-988a-7e736eb40c12'),(21,'/home/shady/Works/attendlyres/attUploads/615359f9-4cbf-4319-a7a5-63638fe5de88_1.jpeg','615359f9-4cbf-4319-a7a5-63638fe5de88'),(22,'/home/shady/Works/attendlyres/attUploads/e78c816e-ffb3-4da6-b73a-df08f5641f3f_1.jpeg','e78c816e-ffb3-4da6-b73a-df08f5641f3f'),(23,'/home/shady/Works/attendlyres/attUploads/efae3eae-a749-4f95-8de7-68814e142bc6_1.jpeg','efae3eae-a749-4f95-8de7-68814e142bc6'),(24,'/home/shady/Works/attendlyres/attUploads/c5895037-0515-4c34-aa6d-2942e907b297_1.jpeg','c5895037-0515-4c34-aa6d-2942e907b297'),(25,'/home/shady/Works/attendlyres/attUploads/b26f8f54-c3a6-4b56-844a-694a6363bcd7_1.jpeg','b26f8f54-c3a6-4b56-844a-694a6363bcd7'),(26,'/home/shady/Works/attendlyres/attUploads/3a59f008-b6cf-4671-b282-2cd2f04a7c1a_1.jpeg','3a59f008-b6cf-4671-b282-2cd2f04a7c1a'),(27,'/home/shady/Works/attendlyres/attUploads/e0c38060-ab77-4481-9db3-cf0fc50d2f9c_1.jpeg','e0c38060-ab77-4481-9db3-cf0fc50d2f9c'),(28,'/home/shady/Works/attendlyres/attUploads/f3a04ef2-d737-47a7-934f-b97e430718c2_1.jpeg','f3a04ef2-d737-47a7-934f-b97e430718c2'),(29,'/home/shady/Works/attendlyres/attUploads/706a8fd2-04b3-40e6-8b0f-9a1b1f12c462_1.jpeg','706a8fd2-04b3-40e6-8b0f-9a1b1f12c462'),(30,'/home/shady/Works/attendlyres/attUploads/4688d829-059a-429a-a475-659f40407051_1.jpeg','4688d829-059a-429a-a475-659f40407051'),(31,'/home/shady/Works/attendlyres/attUploads/d7a66478-c084-44ca-a182-a55501234228_1.jpeg','d7a66478-c084-44ca-a182-a55501234228'),(32,'/home/shady/Works/attendlyres/attUploads/fc2d3097-94f3-4f67-aca0-c58ec6ae56c0_1.jpeg','fc2d3097-94f3-4f67-aca0-c58ec6ae56c0'),(33,'/home/shady/Works/attendlyres/attUploads/0e086e32-84d7-4d4c-a152-f28f372b9650_1.jpeg','0e086e32-84d7-4d4c-a152-f28f372b9650'),(34,'/home/shady/Works/attendlyres/attUploads/3fbdea11-1e78-42e6-91be-e66e97c5329a_1.jpeg','3fbdea11-1e78-42e6-91be-e66e97c5329a'),(35,'/home/shady/Works/attendlyres/attUploads/a33b1129-0eb7-4865-9459-5be329c3ecab_1.jpeg','a33b1129-0eb7-4865-9459-5be329c3ecab'),(36,'/home/shady/Works/attendlyres/attUploads/dc1bd077-b51f-4079-8918-f3eac176bd25_1.jpeg','dc1bd077-b51f-4079-8918-f3eac176bd25'),(37,'/home/shady/Works/attendlyres/attUploads/f195f3b5-7005-4833-92a8-cf95d9f12ffc_1.jpeg','f195f3b5-7005-4833-92a8-cf95d9f12ffc'),(38,'/home/shady/Works/attendlyres/attUploads/776081a5-376c-4a9b-aa88-2386179b612a_1.jpeg','776081a5-376c-4a9b-aa88-2386179b612a'),(39,'/home/shady/Works/attendlyres/attUploads/2834018f-6c10-4280-b699-b0e17f0627bc_1.jpeg','2834018f-6c10-4280-b699-b0e17f0627bc'),(40,'/home/shady/Works/attendlyres/attUploads/fdf333ae-8ab1-4a9b-9bf9-3231be283ede_1.jpeg','fdf333ae-8ab1-4a9b-9bf9-3231be283ede'),(41,'/home/shady/Works/attendlyres/attUploads/4d06ca36-4f82-4f1e-8ea3-88818631e2d5_1.jpeg','4d06ca36-4f82-4f1e-8ea3-88818631e2d5'),(42,'/home/shady/Works/attendlyres/attUploads/7c976118-928e-4265-b098-3d590bb30247_1.jpeg','7c976118-928e-4265-b098-3d590bb30247'),(43,'/home/shady/Works/attendlyres/attUploads/cceb2dc0-4b7d-449a-972c-3ead2281dbd9_1.jpeg','cceb2dc0-4b7d-449a-972c-3ead2281dbd9'),(44,'/home/shady/Works/attendlyres/attUploads/6c97d1ca-82ec-4884-930b-e6412c2ab5a5_1.jpeg','6c97d1ca-82ec-4884-930b-e6412c2ab5a5'),(45,'/home/shady/Works/attendlyres/attUploads/26b02b70-294e-4beb-8a1f-a1e54b867295_1.jpeg','26b02b70-294e-4beb-8a1f-a1e54b867295'),(46,'/home/shady/Works/attendlyres/attUploads/2570d4e7-ea76-4432-ab9e-6d44160b9303_1.jpeg','2570d4e7-ea76-4432-ab9e-6d44160b9303'),(47,'/home/shady/Works/attendlyres/attUploads/9995b525-d7cc-45fa-b2c9-df43803de034_1.jpeg','9995b525-d7cc-45fa-b2c9-df43803de034'),(48,'/home/shady/Works/attendlyres/attUploads/a55fb369-5226-466a-9b36-85a4ed8220ec_1.jpeg','a55fb369-5226-466a-9b36-85a4ed8220ec'),(49,'/home/shady/Works/attendlyres/attUploads/8bc96b9d-629e-4bfe-9456-85dd402affcc_1.jpeg','8bc96b9d-629e-4bfe-9456-85dd402affcc'),(50,'/home/shady/Works/attendlyres/attUploads/f0c03e8e-84dd-4099-b50f-28f076a321a7_1.jpeg','f0c03e8e-84dd-4099-b50f-28f076a321a7'),(51,'/home/shady/Works/attendlyres/attUploads/4e02884b-8e8c-41a1-8d50-918f912f8c12_1.jpeg','4e02884b-8e8c-41a1-8d50-918f912f8c12'),(53,'/home/shady/Works/attendlyres/attUploads/2e158016-81c6-4489-89ab-79fad963f524_1.jpeg','2e158016-81c6-4489-89ab-79fad963f524');
/*!40000 ALTER TABLE `attendance_photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `course` (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `academic_session` varchar(50) DEFAULT NULL,
  `instructor_id` int DEFAULT NULL,
  `trained` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `instructor_id` (`instructor_id`),
  CONSTRAINT `course_ibfk_1` FOREIGN KEY (`instructor_id`) REFERENCES `faculty` (`id`),
  CONSTRAINT `course_chk_1` CHECK ((`trained` in (0,1)))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course`
--

LOCK TABLES `course` WRITE;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` VALUES (1,'cse20','CSE20','2019-2020',1,1);
/*!40000 ALTER TABLE `course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enrolled`
--

DROP TABLE IF EXISTS `enrolled`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `enrolled` (
  `student_id` int NOT NULL,
  `course_id` int NOT NULL,
  PRIMARY KEY (`student_id`,`course_id`),
  KEY `course_id` (`course_id`),
  CONSTRAINT `enrolled_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`),
  CONSTRAINT `enrolled_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enrolled`
--

LOCK TABLES `enrolled` WRITE;
/*!40000 ALTER TABLE `enrolled` DISABLE KEYS */;
INSERT INTO `enrolled` VALUES (2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1);
/*!40000 ALTER TABLE `enrolled` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faculty`
--

DROP TABLE IF EXISTS `faculty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `faculty` (
  `id` int NOT NULL AUTO_INCREMENT,
  `public_id` varchar(50) DEFAULT NULL,
  `email_id` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `password` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_id` (`email_id`),
  UNIQUE KEY `public_id` (`public_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faculty`
--

LOCK TABLES `faculty` WRITE;
/*!40000 ALTER TABLE `faculty` DISABLE KEYS */;
INSERT INTO `faculty` VALUES (1,'fac1','fac1@gmail.com','Fac1','fac1'),(2,'28095900-b6b8-41dc-9c6c-94998721512d','fac2@gmail.com','Fac2','fac2');
/*!40000 ALTER TABLE `faculty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marked`
--

DROP TABLE IF EXISTS `marked`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `marked` (
  `attendance_id` int NOT NULL,
  `student_id` int NOT NULL,
  KEY `attendance_id` (`attendance_id`),
  KEY `student_id` (`student_id`),
  CONSTRAINT `marked_ibfk_1` FOREIGN KEY (`attendance_id`) REFERENCES `attendance` (`id`),
  CONSTRAINT `marked_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marked`
--

LOCK TABLES `marked` WRITE;
/*!40000 ALTER TABLE `marked` DISABLE KEYS */;
INSERT INTO `marked` VALUES (17,2),(17,9),(17,7),(17,8),(17,3),(17,6),(17,5),(17,4),(18,2),(18,9),(18,5),(18,6),(18,4),(18,7),(18,8),(18,3),(19,6),(19,3),(19,9),(19,4),(19,2),(19,8),(19,7),(19,5),(20,3),(20,7),(20,5),(20,6),(20,4),(20,2),(20,9),(20,8),(21,4),(21,9),(21,3),(21,8),(21,2),(21,7),(21,6),(21,5),(22,9),(22,8),(22,2),(22,3),(22,6),(22,5),(22,7),(22,4),(23,6),(23,2),(23,8),(23,5),(23,3),(23,9),(23,4),(23,7),(24,4),(24,5),(24,3),(24,2),(24,8),(24,6),(24,7),(24,9),(25,9),(25,8),(25,4),(25,6),(25,2),(25,7),(25,5),(25,3),(26,3),(26,9),(26,8),(26,2),(26,6),(26,4),(26,7),(26,5),(28,9),(28,8),(28,4),(28,5),(28,2),(28,7),(28,6),(28,3);
/*!40000 ALTER TABLE `marked` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photo`
--

DROP TABLE IF EXISTS `photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `photo` (
  `id` int NOT NULL AUTO_INCREMENT,
  `uri` text,
  `student_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`),
  CONSTRAINT `photo_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photo`
--

LOCK TABLES `photo` WRITE;
/*!40000 ALTER TABLE `photo` DISABLE KEYS */;
/*!40000 ALTER TABLE `photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student` (
  `id` int NOT NULL AUTO_INCREMENT,
  `regno` varchar(9) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `email_id` varchar(50) NOT NULL,
  `password` varchar(80) DEFAULT NULL,
  `emg_contact` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_id` (`email_id`),
  UNIQUE KEY `regno` (`regno`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES (1,'stud1','Stud1','stud1@gmail.com','stud1',NULL),(2,'B17001CS','Akhil Babu','akhil@gmail.com','1593',NULL),(3,'B17002CS','Anfas','anfas@gmail.com','1593',NULL),(4,'B17003CS','Abdul Basith','basith@gmail.com','1593',NULL),(5,'B17004CS','Harijothis','hari@gmail.com','1593',NULL),(6,'B17005CS','Rahul Biji','biji@gmail.com','1593',NULL),(7,'B17006CS','Rayhan','rayhan@gmail.com','1593',NULL),(8,'B17007CS','Salih','salih@gmail.com','1593',NULL),(9,'B17008CS','Shuhaib','shuhaib@gmail.com','1593',NULL),(10,'B170282CS','Afeedh','afeedh@gmail.com','1593','afeedhdetroit@gmail.com');
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket`
--

DROP TABLE IF EXISTS `ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ticket` (
  `id` int NOT NULL AUTO_INCREMENT,
  `instructor_id` int DEFAULT NULL,
  `course_id` int DEFAULT NULL,
  `issue` varchar(100) DEFAULT NULL,
  `pending` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `course_id` (`course_id`),
  KEY `instructor_id` (`instructor_id`),
  CONSTRAINT `ticket_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`),
  CONSTRAINT `ticket_ibfk_2` FOREIGN KEY (`instructor_id`) REFERENCES `faculty` (`id`),
  CONSTRAINT `ticket_chk_1` CHECK ((`pending` in (0,1)))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket`
--

LOCK TABLES `ticket` WRITE;
/*!40000 ALTER TABLE `ticket` DISABLE KEYS */;
INSERT INTO `ticket` VALUES (1,1,1,'\"Not recognizing faces of cse20\"',1);
/*!40000 ALTER TABLE `ticket` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-31 15:15:43
