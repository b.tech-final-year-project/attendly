"""empty message

Revision ID: 92a10ec92fee
Revises: e3908143b05b
Create Date: 2021-03-04 14:27:47.155377

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '92a10ec92fee'
down_revision = 'e3908143b05b'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('ticketstatus', sa.Column('id', sa.Integer(), nullable=False))
    op.alter_column('ticketstatus', 'status_code',
               existing_type=mysql.INTEGER(),
               nullable=True)
    op.create_unique_constraint(None, 'ticketstatus', ['status_code'])
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'ticketstatus', type_='unique')
    op.alter_column('ticketstatus', 'status_code',
               existing_type=mysql.INTEGER(),
               nullable=False)
    op.drop_column('ticketstatus', 'id')
    # ### end Alembic commands ###
