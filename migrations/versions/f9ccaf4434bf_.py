"""empty message

Revision ID: f9ccaf4434bf
Revises: 0d86ab994e9e
Create Date: 2021-03-08 17:33:39.185617

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = 'f9ccaf4434bf'
down_revision = '0d86ab994e9e'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('ticket', 'created_by',
               existing_type=mysql.INTEGER(),
               nullable=False)
    op.alter_column('ticket', 'status_code',
               existing_type=mysql.INTEGER(),
               nullable=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('ticket', 'status_code',
               existing_type=mysql.INTEGER(),
               nullable=True)
    op.alter_column('ticket', 'created_by',
               existing_type=mysql.INTEGER(),
               nullable=True)
    # ### end Alembic commands ###
