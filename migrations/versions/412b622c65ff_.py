"""empty message

Revision ID: 412b622c65ff
Revises: f9ccaf4434bf
Create Date: 2021-04-07 22:10:15.160394

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '412b622c65ff'
down_revision = 'f9ccaf4434bf'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('course', sa.Column('threshold', sa.Float(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('course', 'threshold')
    # ### end Alembic commands ###
