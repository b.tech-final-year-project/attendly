# Attendly v0.1

## Pre-requisites

- Python 3.5 
  
If you're on Debian based linux, follow these steps to install Python 3.5 :


    $ sudo add-apt-repository ppa:deadsnakes/ppa

```
$ sudo apt-get update
```

```
$ sudo apt-get install python3.5
```

-  Virtualenv

For Debian based distros:

```
$ sudo apt install virtualenv
```

## Development Setup

1. Go to the project root and create a virtual environment for the setup
( Path to python 3.5 is usually /usr/bin/python3.5 )

```
$ virtualenv -p <path/to/python3.5> venv
```

2. Activate the virtual environment 
```
$ source venv/bin/activate
```

3. Install the requirements
```
$ pip install -r requirements.txt
```

4. Replace thr `app/config.py` with your own local server details


5. With the above application you can create the database or enable migrations if the database already exists with the following command:
```
    $ python manage.py db init
```

6. You can then generate an initial migration:
```
    $ python manage.py db migrate
```

7. Then you can apply the migration to the database:
```
    $ python manage.py db upgrade
```

8. Start the server using the following command:
```
    $ python server.py
```

## To run as docker container

1. Replace the `docker-compose.yml.sample` with your own docker-compose yaml file.

2. Run docker-compose
```
$ docker-compose up
```

3. To tear down the container
```
$ docker-compose down
```

4. To re-build the docker container
```
$ docker-compose build

```

