import flask_migrate
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager, Server
from apps import app, db

manager = Manager(app)
migrate = Migrate(app, db)

manager.add_command("db", MigrateCommand)

manager.add_command("debugserver", Server(use_debugger=True, use_reloader=True, host="0.0.0.0", port=8000))

manager.add_command("runserver", Server(host="0.0.0.0", port=8000))


@manager.command
def init_first():
    flask_migrate.init()
    flask_migrate.migrate()
    flask_migrate.upgrade()


if __name__ == "__main__":
    manager.run()
