FROM python:3.5-slim
WORKDIR /attendly
RUN apt-get update && apt-get install gcc libgtk2.0-dev -y python-mysqldb libc-dev default-libmysqlclient-dev  && apt-get clean && apt-get install python-matplotlib
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
EXPOSE 6000
COPY . .
CMD ["python", "server.py"]
